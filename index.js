
// class Dog {

//     constructor(name,breed,age){

//         this.name = name;
//         this.breed = breed;
//         this.age = age * 7;

//     }
// };


// let dog1 = new Dog("Bantay","chihuahua",3);

// console.log(dog1);

// class Person {

//     constructor(name,age,nationality,address){
//         this.name = name;
//         this.nationality = nationality;
//         this.address = address;

//         typeof age !== "number" ? this.age = undefined : this.age = age
//     }
//     greet(){
//         console.log(`Hello! Good Morning!`)
//         return "Jamie"; //sholud return this.
//     };
//     introduce(){
//         console.log(`Hi! My name is ${this.name}`)
//         return this;
//     };
//     changeAddress(newAddress){
//         this.address = newAddress;
//         return this;
//     };
// }

// let person1 = new Person("Luffy",21,"Pirate","East Blue");
// let person2 = new Person("Mervin", true, "Filipino", "Cavite");

// console.log(person1);
// console.log(person2);

// person1.greet().introduce();
// person2.greet().introduce();
    
// ********* ACTIVITY *************
class Student {

    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.average = undefined;
        this.isPassed = undefined;
        this.isPassedWithHonors = undefined;
        if(grades.every(grade => typeof grade !== `number` )) ? this.grades = undefined : this.grades = grade ;
    }

    login(){
        console.log(`${this.email} has logged in`);
    }

    logout(){
        console.log(`${this.email} has logged out`);
    }

    listGrades(){
        this.grades.forEach(grade => {
        return this;
        })   
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/grades.length);
        return this;
    }

    willPass(){
        this.isPassed = Math.round(this.computeAve()) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors(){
        this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
        return this;
    }

}


let student1 = new Student("John","john@mail.com",[89, 84, 78, 88]);

let student2 = new Student("Joe","joe@mail.com",[78, 82, 79, 85]);

let student3 = new Student("Jane","jane@mail.com",[87, 89, 91, 93]);

let student4 = new Student("Jessie","jessie@mail.com",[91, 89, 92, 93]);


console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);

